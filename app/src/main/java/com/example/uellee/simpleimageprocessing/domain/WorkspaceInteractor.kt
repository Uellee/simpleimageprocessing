package com.example.uellee.simpleimageprocessing.domain

import com.example.uellee.simpleimageprocessing.data.IResponseSource
import com.example.uellee.simpleimageprocessing.domain.model.Transformation
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by DEA on 20.12.17.
 */
class WorkspaceInteractor @Inject constructor(
        private val repository: IRepository,
        private val implicitRouter: IImplicitRouter,
        responseSource: IResponseSource
) : IWorkspaceUseCases {
    override val transformationStream: Observable<Pair<Int, Transformation>>
        get() = repository.appliedTransformationsStream

    override val bitmapUriStream: Observable<String>
        get() = repository.imageStream

    override val cachedTransformations: List<Pair<Int, Transformation>>
        get() = repository.cachedTransformations

    init {
        repository.responseSource = responseSource
    }

    override fun openImageFromGallery() {
        implicitRouter.dispatchImageIntent()
    }

    override fun openImageFromCamera() {
        implicitRouter.dispatchCameraIntent(repository.tempImageUri)
    }

    override fun rotate() {
        repository.applyTransformation(Transformation.POSITIVE_ROTATION)
    }

    override fun mirror() {
        repository.applyTransformation(Transformation.MIRROR)
    }

    override fun grayScale() {
        repository.applyTransformation(Transformation.GRAY_SCALE)
    }

    override fun removeTransformation(id: Int) {
        repository.removeTransformation(id)
    }

    override fun useTransformedImageAsSource(id: Int) {
        repository.setTransformedImageAsSource(id)
    }
}