package com.example.uellee.simpleimageprocessing.di.activity

import com.example.uellee.simpleimageprocessing.domain.IWorkspaceUseCases
import com.example.uellee.simpleimageprocessing.domain.WorkspaceInteractor
import dagger.Binds
import dagger.Module

/**
 * Created by DEA on 20.12.17.
 */
@Module
interface UseCasesModule {
    @Binds
    @ActivityScope
    fun provideWorkspaceInteractor(workspaceInteractor: WorkspaceInteractor): IWorkspaceUseCases
}