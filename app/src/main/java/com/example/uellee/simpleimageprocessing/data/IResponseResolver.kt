package com.example.uellee.simpleimageprocessing.data

import android.content.Intent
import android.net.Uri
import io.reactivex.Observable

/**
 * Created by DEA on 20.12.17.
 */
interface IResponseResolver {
    val contentUriStream: Observable<Uri>
    var responseSource: IResponseSource?

    fun resolve(requestCode: Int, resultCode: Int, data: Intent?)
}