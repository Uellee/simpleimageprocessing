package com.example.uellee.simpleimageprocessing.di

import com.example.uellee.simpleimageprocessing.di.activity.ActivityComponent
import com.example.uellee.simpleimageprocessing.di.app.AppComponent
import com.example.uellee.simpleimageprocessing.di.workspace.WorkspaceComponent

/**
 * Created by DEA on 17.12.17.
 * contains dagger 2 components
 */
object ComponentsOwner {
    lateinit var appComponent: AppComponent
    lateinit var activityComponent: ActivityComponent
    lateinit var workspaceComponent: WorkspaceComponent
}