package com.example.uellee.simpleimageprocessing.di.activity

import com.example.uellee.simpleimageprocessing.di.workspace.WorkspaceComponent
import com.example.uellee.simpleimageprocessing.presentation.BaseActivity
import dagger.Subcomponent

/**
 * Created by DEA on 20.12.17.
 */
@ActivityScope
@Subcomponent(modules = [ActivityModule::class,
    ImplicitRouterModule::class,
    RepositoryModule::class,
    UseCasesModule::class])
interface ActivityComponent {
    fun inject(baseActivity: BaseActivity)

    fun workspaceComponentBuilder(): WorkspaceComponent.Builder

    @Subcomponent.Builder
    interface Builder {
        fun build(): ActivityComponent
        fun activityModule(activityModule: ActivityModule): ActivityComponent.Builder
        fun usecasesModule(useCasesModule: UseCasesModule): ActivityComponent.Builder
        fun implicitRouterModule(implicitRouterModule: ImplicitRouterModule): ActivityComponent.Builder
        fun repositoryModule(repositoryModule: RepositoryModule): ActivityComponent.Builder
    }

//    fun plusWorkspaceComponent(workspaceViewModule: WorkspaceViewModule): WorkspaceComponent
}