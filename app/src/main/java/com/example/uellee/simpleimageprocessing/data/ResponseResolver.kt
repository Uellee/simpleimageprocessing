package com.example.uellee.simpleimageprocessing.data

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import com.example.uellee.simpleimageprocessing.presentation.implicitinteraction.RequestCodes
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

/**
 * Created by DEA on 21.12.17.
 */
class ResponseResolver @Inject constructor(val context: Context,
                                           val fileManager: IFileManager) : IResponseResolver {
    override var responseSource: IResponseSource? = null
        set(value) {
            value?.responseStream?.subscribe {
                resolve(it.requestCode, it.resultCode, it.data)
            }
            field = value
        }

    private val uriSubject = BehaviorSubject.create<Uri>()
    override val contentUriStream: Observable<Uri>
        get() = uriSubject.hide()


    override fun resolve(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode != AppCompatActivity.RESULT_OK) return

        when (requestCode) {
            RequestCodes.REQUEST_IMAGE_FROM_GALLERY -> data?.let { pickGalleryUri(it) }
            RequestCodes.REQUEST_CAMERA_CAPTURE -> pickCameraCaptureUri()
        }
    }

    private fun pickGalleryUri(data: Intent) {
        uriSubject.onNext(data.data)
    }

    private fun pickCameraCaptureUri() {
        fileManager.cacheUri?.let { uriSubject.onNext(it) }
    }

}