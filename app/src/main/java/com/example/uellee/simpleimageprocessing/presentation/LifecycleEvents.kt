package com.example.uellee.simpleimageprocessing.presentation

/**
 * Created by DEA on 18.12.17.
 */
enum class LifecycleEvents {
    ON_CREATE, ON_START, ON_RESUME, ON_PAUSE, ON_STOP, ON_DESTROY
}