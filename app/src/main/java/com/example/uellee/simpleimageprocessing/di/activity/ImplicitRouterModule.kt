package com.example.uellee.simpleimageprocessing.di.activity

import com.example.uellee.simpleimageprocessing.domain.IImplicitRouter
import com.example.uellee.simpleimageprocessing.presentation.implicitinteraction.ImplicitRouter
import dagger.Binds
import dagger.Module

/**
 * Created by DEA on 20.12.17.
 */
@Module
interface ImplicitRouterModule {
    @Binds
    @ActivityScope
    fun bindImplicitRouter(implicitRouter: ImplicitRouter): IImplicitRouter
}