package com.example.uellee.simpleimageprocessing.domain

import com.example.uellee.simpleimageprocessing.data.IResponseSource
import com.example.uellee.simpleimageprocessing.domain.model.Transformation
import io.reactivex.Observable

/**
 * Created by DEA on 20.12.17.
 * @property imageStream emits uriString of a processing currentImage
 * @property tempImageUri provides a uriString of a temporary currentImage buffer file
 * @property appliedTransformationsStream emits transformation transformation & its identification number
 *
 * @property responseSource link is used as a source of activity result emits
 */
interface IRepository {
    val imageStream: Observable<String>
    val tempImageUri: String
    val appliedTransformationsStream: Observable<Pair<Int, Transformation>>
    val cachedTransformations: List<Pair<Int, Transformation>>

    var responseSource: IResponseSource?
    fun setTransformedImageAsSource(id: Int)
    fun applyTransformation(transformation: Transformation)
    fun removeTransformation(id: Int)
}