package com.example.uellee.simpleimageprocessing.presentation.implicitinteraction

/**
 * Created by DEA on 21.12.17.
 */
object RequestCodes {
    const val REQUEST_IMAGE_FROM_GALLERY = 2
    const val REQUEST_CAMERA_CAPTURE = 3
}