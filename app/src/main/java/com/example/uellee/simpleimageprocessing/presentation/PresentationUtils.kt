package com.example.uellee.simpleimageprocessing.presentation

import android.content.Context
import android.view.LayoutInflater
import android.view.View

/**
 * Created by DEA on 05.01.18.
 */
fun View.onClick(onClick: () -> Unit) {
    setOnClickListener { onClick() }
}

val Context.layoutInflater get() = LayoutInflater.from(this)