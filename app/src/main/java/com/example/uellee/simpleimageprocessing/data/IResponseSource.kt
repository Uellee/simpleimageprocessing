package com.example.uellee.simpleimageprocessing.data

import android.content.Intent
import io.reactivex.Observable

/**
 * Created by DEA on 24.12.17.
 */
interface IResponseSource {
    val responseStream: Observable<Response>
}

data class Response(val requestCode: Int, val resultCode: Int, val data: Intent?)