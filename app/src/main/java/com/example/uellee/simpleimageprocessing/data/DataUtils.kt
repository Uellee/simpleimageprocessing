package com.example.uellee.simpleimageprocessing.data

import android.util.SparseArray
import com.example.uellee.simpleimageprocessing.domain.model.Transformation
import java.util.*

/**
 * Created by DEA on 29.12.17.
 */

fun <E> SparseArray<E>.put(pair: Pair<Int, E>) {
    pair.apply { put(first, second) }
}

/**
 * @see https://github.com/Kotlin/anko/blob/master/anko/library/static/commons/src/collections/SparseArrays.kt
 * */
inline fun <E> SparseArray<E>.forEach(action: (Int, E) -> Unit) {
    val size = this.size()
    for (i in 0 until size) {
        if (size != this.size()) throw ConcurrentModificationException()
        action(this.keyAt(i), this.valueAt(i))
    }
}

fun buildArrayList(m: SparseArray<Transformation>): ArrayList<Pair<Int, Transformation>> {
    val result: ArrayList<Pair<Int, Transformation>> = ArrayList()
    m.forEach { id, transformation -> result.add((id to transformation)) }
    return result
}