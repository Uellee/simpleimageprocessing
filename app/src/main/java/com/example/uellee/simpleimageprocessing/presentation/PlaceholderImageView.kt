package com.example.uellee.simpleimageprocessing.presentation

import android.content.Context
import android.graphics.Bitmap
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import com.example.uellee.simpleimageprocessing.R
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.image_placeholder.view.*
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by DEA on 02.01.18.
 */
class PlaceholderImageView @JvmOverloads constructor(context: Context,
                                                     attrs: AttributeSet? = null,
                                                     defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private val progress by lazy { pb_transformation }
    private val transformedImageView by lazy { iv_transformed_image }
    private val rnd: Long
        get() {
            var l = Random().nextLong() % 25
            if (l < 0) l *= -1
            return l + 5
        }

    init {
        View.inflate(context, R.layout.image_placeholder, this)
    }

    fun setBitmapSource(transformationStream: Single<Bitmap?>) {
        transformationStream
                .delay(rnd, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { bm ->
                    transformedImageView.setImageBitmap(bm)
                    progress.visibility = View.GONE
                    transformedImageView.visibility = View.VISIBLE
                }
    }

    fun reset() {
        progress.visibility = View.VISIBLE
        transformedImageView.visibility = View.GONE
    }
}