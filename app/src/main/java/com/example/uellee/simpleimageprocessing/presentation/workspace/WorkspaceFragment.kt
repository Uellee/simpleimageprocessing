package com.example.uellee.simpleimageprocessing.presentation.workspace


import android.graphics.Bitmap
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.example.uellee.simpleimageprocessing.R
import com.example.uellee.simpleimageprocessing.di.ComponentsOwner
import com.example.uellee.simpleimageprocessing.di.workspace.WorkspaceViewModule
import com.example.uellee.simpleimageprocessing.presentation.LifecycleEvents
import com.example.uellee.simpleimageprocessing.presentation.onClick
import com.example.uellee.simpleimageprocessing.presentation.transformationslist.TransformationsAdapter
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_workspace.*
import javax.inject.Inject

class WorkspaceFragment : Fragment(), WorkspacePresenter.IWorkspaceView {
    @Inject lateinit var presenter: WorkspacePresenter

    private val lifecycleSubject = PublishSubject.create<LifecycleEvents>()

    override val lifecycleStream: Observable<LifecycleEvents>
        get() = lifecycleSubject.hide()

    private val currentImage by lazy { i_current_image }
    private lateinit var lTransformationPanel: View

    private lateinit var lNavigationPanel: View

    private lateinit var adapter: TransformationsAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ComponentsOwner.workspaceComponent = ComponentsOwner.activityComponent
                .workspaceComponentBuilder()
                .workspaceViewModule(WorkspaceViewModule(this))
                .build()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_workspace, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val recyclerView = rv_transformations_list
        lTransformationPanel = l_transformation_panel
        lNavigationPanel = view.findViewById<FrameLayout>(R.id.l_navigation_panel)

        ComponentsOwner.workspaceComponent.inject(this)

        currentImage.viewTreeObserver.addOnGlobalLayoutListener {
            presenter.imageWidth = currentImage.width
        }

        adapter = TransformationsAdapter(
                presenter.transformations,
                presenter::onTransformationItemClick,
                presenter::onRemoveItem)
        recyclerView.adapter = adapter

        currentImage.onClick(presenter::onCurrentImageClick)

        i_b_rotate.onClick(presenter::onRotateClick)
        i_b_mirror.onClick(presenter::onMirrorClick)
        i_b_grayscale.onClick(presenter::onGrayScaleClick)

        i_b_camera.onClick(presenter::onCameraRequest)
        i_b_gallery.onClick(presenter::onGalleryRequest)
    }

    override fun addTransformation(id: Int, image: Single<Bitmap?>) {
        adapter.addItem(id to image)
    }

    override fun setImage(bitmap: Bitmap) {
        currentImage.setImageBitmap(bitmap)
    }

    override fun showTransformationPanel() {
        lTransformationPanel.visibility = View.VISIBLE
    }

    override fun hideTransformationPanel() {
        lTransformationPanel.visibility = View.GONE
    }

    override fun showNavigationDialog() {
        lNavigationPanel.visibility = View.VISIBLE
    }

    override fun hideNavigationDialog() {
        lNavigationPanel.visibility = View.GONE
    }

    override fun clearTransformationsList() {
        adapter.clear()
    }

    override fun removeItem(id: Int) {
        adapter.removeItem(id)
    }
}
