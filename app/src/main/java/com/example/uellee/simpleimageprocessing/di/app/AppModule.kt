package com.example.uellee.simpleimageprocessing.di.app

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by DEA on 20.12.17.
 */
@Module
class AppModule(val context: Context) {
    @Singleton
    @Provides
    fun provideContext() = context
}