package com.example.uellee.simpleimageprocessing.presentation.workspace

import android.graphics.Bitmap
import android.net.Uri
import com.example.uellee.simpleimageprocessing.domain.IWorkspaceUseCases
import com.example.uellee.simpleimageprocessing.domain.model.Transformation
import com.example.uellee.simpleimageprocessing.presentation.LifecycleEvents
import com.example.uellee.simpleimageprocessing.presentation.transformationslist.BitmapAdapter
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by DEA on 18.12.17.
 */
class WorkspacePresenter @Inject constructor(
        private val view: IWorkspaceView,
        private val interactor: IWorkspaceUseCases,
        private val bitmapAdapter: BitmapAdapter) {

    private val disposables = CompositeDisposable()
    private var noEditingImage = true
    private var editingImage = false

    val transformations = interactor.cachedTransformations.map {
        it.first to when (it.second) {
            Transformation.POSITIVE_ROTATION -> bitmapAdapter.rotatedBitmapStream
            Transformation.MIRROR -> bitmapAdapter.mirrorBitmapStream
            Transformation.GRAY_SCALE -> bitmapAdapter.grayScaleBitmapStream
        }
    }.toMutableList()

    //    round numbers are always helpful
    var imageWidth: Int = 256
        set(value) {
            bitmapAdapter.width = value
        }

    init {
        view.lifecycleStream
                .filter { it == LifecycleEvents.ON_STOP }
                .subscribe { release() }
                .let { disposables.add(it) }

        interactor.bitmapUriStream
                .observeOn(AndroidSchedulers.mainThread())
                .map { Uri.parse(it) }
                .subscribe {
                    noEditingImage = false
                    onCurrentImageClick()
                    if (bitmapAdapter.currentImageUri != it)
                        view.clearTransformationsList()
                    bitmapAdapter.currentImageUri = it
                    bitmapAdapter.originalBitmap
                            ?.let { bm -> view.setImage(bm) }
                }
                .let { disposables.add(it) }

        interactor.transformationStream
                .observeOn(AndroidSchedulers.mainThread())
                .map {
                    when (it.second) {
                        Transformation.POSITIVE_ROTATION ->
                            (it.first to bitmapAdapter.rotatedBitmapStream)
                        Transformation.MIRROR ->
                            (it.first to bitmapAdapter.mirrorBitmapStream)
                        Transformation.GRAY_SCALE ->
                            (it.first to bitmapAdapter.grayScaleBitmapStream)
                    }
                }
                .subscribe {
                    view.addTransformation(it.first, it.second)
                }
                .let { disposables.add(it) }
    }

    private fun release() = disposables.clear()

    fun onCurrentImageClick() {
        if (noEditingImage) return
        editingImage = !editingImage

        if (editingImage) {
            view.hideTransformationPanel()
            view.showNavigationDialog()
        } else {
            view.hideNavigationDialog()
            view.showTransformationPanel()
        }
    }

    fun onCameraRequest() = interactor.openImageFromCamera()

    fun onGalleryRequest() = interactor.openImageFromGallery()

    fun onRotateClick() = interactor.rotate()

    fun onMirrorClick() = interactor.mirror()

    fun onGrayScaleClick() = interactor.grayScale()

    fun onTransformationItemClick(id: Int) = interactor.useTransformedImageAsSource(id)

    fun onRemoveItem(id: Int) {
        view.removeItem(id)
        interactor.removeTransformation(id)
    }

    interface IWorkspaceView {
        val lifecycleStream: Observable<LifecycleEvents>

        fun setImage(bitmap: Bitmap)
        fun addTransformation(id: Int, image: Single<Bitmap?>)
        fun hideTransformationPanel()
        fun showTransformationPanel()
        fun hideNavigationDialog()
        fun showNavigationDialog()
        fun clearTransformationsList()
        fun removeItem(id: Int)

    }
}