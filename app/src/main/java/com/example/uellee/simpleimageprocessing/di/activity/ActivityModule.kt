package com.example.uellee.simpleimageprocessing.di.activity

import android.support.v7.app.AppCompatActivity
import com.example.uellee.simpleimageprocessing.data.IResponseSource
import com.example.uellee.simpleimageprocessing.presentation.BaseActivity
import dagger.Module
import dagger.Provides

/**
 * Created by DEA on 20.12.17.
 */
@Module
class ActivityModule(val activity: BaseActivity) {
    @ActivityScope
    @Provides
    fun provideActivity(): AppCompatActivity = activity

    @ActivityScope
    @Provides
    fun provideResponceSource(): IResponseSource = activity
}
