package com.example.uellee.simpleimageprocessing.domain

import com.example.uellee.simpleimageprocessing.domain.model.Transformation
import io.reactivex.Observable

/**
 * Created by DEA on 18.12.17.
 */
interface IWorkspaceUseCases {
    val bitmapUriStream: Observable<String>
    val transformationStream: Observable<Pair<Int, Transformation>>

    val cachedTransformations: List<Pair<Int, Transformation>>

    fun openImageFromGallery()
    fun openImageFromCamera()

    fun rotate()
    fun mirror()
    fun grayScale()

    fun removeTransformation(id: Int)
    fun useTransformedImageAsSource(id: Int)

}