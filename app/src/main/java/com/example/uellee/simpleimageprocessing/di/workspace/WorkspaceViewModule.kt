package com.example.uellee.simpleimageprocessing.di.workspace

import com.example.uellee.simpleimageprocessing.presentation.workspace.WorkspacePresenter
import dagger.Module
import dagger.Provides

/**
 * Created by DEA on 18.12.17.
 */
@Module
class WorkspaceViewModule(val view: WorkspacePresenter.IWorkspaceView) {
    @Provides
    @WorkspaceScope
    fun provideView() = view
}