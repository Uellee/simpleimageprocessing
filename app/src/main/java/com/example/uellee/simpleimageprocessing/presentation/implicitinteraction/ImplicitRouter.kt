package com.example.uellee.simpleimageprocessing.presentation.implicitinteraction

import android.content.Intent
import android.net.Uri
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import com.example.uellee.simpleimageprocessing.domain.IImplicitRouter
import javax.inject.Inject

/**
 * Created by DEA on 20.12.17.
 */
class ImplicitRouter @Inject constructor(val activity: AppCompatActivity) : IImplicitRouter {
    override fun dispatchImageIntent() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        intent.addCategory(Intent.CATEGORY_OPENABLE)

        sendIntent(intent, RequestCodes.REQUEST_IMAGE_FROM_GALLERY)
    }

    override fun dispatchCameraIntent(tempImageFileUri: String) {
        val intent = Intent()
        intent.action = MediaStore.ACTION_IMAGE_CAPTURE
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.parse(tempImageFileUri))

        sendIntent(intent, RequestCodes.REQUEST_CAMERA_CAPTURE)
    }

    private fun sendIntent(intent: Intent, requestCode: Int) {
        if (intent.resolveActivity(activity.packageManager) != null)
            activity.startActivityForResult(intent, requestCode)
    }

}