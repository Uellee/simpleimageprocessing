package com.example.uellee.simpleimageprocessing.di.app

import com.example.uellee.simpleimageprocessing.data.IResponseResolver
import com.example.uellee.simpleimageprocessing.data.ResponseResolver
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

/**
 * Created by DEA on 21.12.17.
 */
@Module
interface ResponseResolverModule {
    @Binds
    @Singleton
    fun bindResponseResolver(implicitResolver: ResponseResolver): IResponseResolver
}