package com.example.uellee.simpleimageprocessing.di.app

import com.example.uellee.simpleimageprocessing.data.FileManager
import com.example.uellee.simpleimageprocessing.data.IFileManager
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

/**
 * Created by DEA on 24.12.17.
 */
@Module
interface FileManagerModule {
    @Binds
    @Singleton
    fun bindFileManager(fileManager: FileManager): IFileManager
}