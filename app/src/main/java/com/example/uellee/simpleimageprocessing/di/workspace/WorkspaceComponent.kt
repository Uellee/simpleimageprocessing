package com.example.uellee.simpleimageprocessing.di.workspace

import com.example.uellee.simpleimageprocessing.presentation.workspace.WorkspaceFragment
import dagger.Subcomponent

/**
 * Created by DEA on 18.12.17.
 */
@WorkspaceScope
@Subcomponent(modules = [WorkspaceViewModule::class])
interface WorkspaceComponent {
    fun inject(workspaceFragment: WorkspaceFragment)

    @Subcomponent.Builder
    interface Builder {
        fun build(): WorkspaceComponent
        fun workspaceViewModule(workspaceViewModule: WorkspaceViewModule): WorkspaceComponent.Builder
    }
}