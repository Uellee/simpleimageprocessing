package com.example.uellee.simpleimageprocessing.presentation.transformationslist

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Rect
import android.net.Uri
import com.example.uellee.simpleimageprocessing.presentation.grayScale
import com.example.uellee.simpleimageprocessing.presentation.mirror
import com.example.uellee.simpleimageprocessing.presentation.rotate
import io.reactivex.Scheduler
import io.reactivex.Single
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by DEA on 02.01.18.
 * Keeps already applied transformations im memory
 * @property currentImageUri master should be set before applying transformations
 */
@Singleton
class BitmapAdapter @Inject constructor(
        @Named("CachedThreadPoolScheduler") private val scheduler: Scheduler,
        val context: Context
) {

    var currentImageUri: Uri? = null
        set(value) {

            when {
                value == null -> {
                    field = null
                    return
                }
                value == field ->
                    return
                field != null ->
                    resetCache()
            }

            /*Should be applied before calcISS call*/
            field = value

            openBitmap()
        }

    private var inSampleSize = 1
        set(value) {
            field = if (value < 1) 1 else value
        }
    /*What value could be set as default?*/
    var width: Int = 100
        set(value) {
            if (value == field) return

            field = value
            if (currentImageUri != null) {
//                resetCache()
                calcInSampleSize()
                openBitmap()
            }
        }


    private var masterBitmap: Bitmap? = null
    val originalBitmap: Bitmap?
        get() = masterBitmap

    private var rotatedBitmapCache: Bitmap? = null
        get() = field ?: cacheRotation()
    val rotatedBitmapStream: Single<Bitmap?>
        get() = Single.fromCallable { rotatedBitmapCache }
                .timeout(3, TimeUnit.SECONDS)
                .subscribeOn(scheduler)

    private var mirrorBitmapCache: Bitmap? = null
        get() = field ?: cacheMirroring()
    val mirrorBitmapStream: Single<Bitmap?>
        get() = Single.fromCallable { mirrorBitmapCache }
                .timeout(3, TimeUnit.SECONDS)
                .subscribeOn(scheduler)

    private var grayScaleBitmapCache: Bitmap? = null
        get() = field ?: cacheGrayScaling()
    val grayScaleBitmapStream: Single<Bitmap?>
        get() = Single.fromCallable { grayScaleBitmapCache }
                .timeout(3, TimeUnit.SECONDS)
                .subscribeOn(scheduler)


    private fun cacheRotation(): Bitmap? {
        rotatedBitmapCache = masterBitmap?.rotate()
        return rotatedBitmapCache
    }

    private fun cacheMirroring(): Bitmap? {
        mirrorBitmapCache = masterBitmap?.mirror()
        return mirrorBitmapCache
    }

    private fun cacheGrayScaling(): Bitmap? {
        grayScaleBitmapCache = masterBitmap?.grayScale()
        return grayScaleBitmapCache
    }

    private fun resetCache() {
        masterBitmap = null
        rotatedBitmapCache = null
        mirrorBitmapCache = null
        grayScaleBitmapCache = null
    }

    private fun calcInSampleSize() {
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        val stream = context.contentResolver.openInputStream(currentImageUri)
        BitmapFactory.decodeStream(stream, Rect(-1, -1, -1, -1), options)

        inSampleSize = options.outWidth / width
    }

    private fun openBitmap() {
        calcInSampleSize()

        val options = BitmapFactory.Options()
        options.inSampleSize = inSampleSize

        val stream = context.contentResolver.openInputStream(currentImageUri)
        masterBitmap = BitmapFactory.decodeStream(
                stream, Rect(-1, -1, -1, -1), options)
    }
}