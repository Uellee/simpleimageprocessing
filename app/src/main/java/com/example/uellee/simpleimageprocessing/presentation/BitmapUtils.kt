package com.example.uellee.simpleimageprocessing.presentation

import android.graphics.*
import com.example.uellee.simpleimageprocessing.domain.model.Transformation


/**
 * Created by DEA on 25.12.17.
 */
fun Bitmap.rotate(): Bitmap {
    val matrix = Matrix()
    matrix.preRotate(90f)
    return Bitmap.createBitmap(this, 0, 0, width, height, matrix, true)
}

fun Bitmap.mirror(): Bitmap {
    val matrix = Matrix()
    matrix.preScale(-1f, 1f)

    return Bitmap.createBitmap(this, 0, 0, width, height, matrix, true)
}

fun Bitmap.grayScale(): Bitmap {
    val width = this.width
    val height = this.height

    val bitmapResult = Bitmap
            .createBitmap(width, height, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(bitmapResult)
    val paint = Paint()
    val colorMatrix = ColorMatrix()
    colorMatrix.setSaturation(0f)
    val filter = ColorMatrixColorFilter(colorMatrix)
    paint.colorFilter = filter
    canvas.drawBitmap(this, 0f, 0f, paint)

    return bitmapResult
}

fun Bitmap.transform(transformation: Transformation): Bitmap = when (transformation) {
    Transformation.GRAY_SCALE -> this.grayScale()
    Transformation.MIRROR -> this.mirror()
    Transformation.POSITIVE_ROTATION -> this.rotate()
}