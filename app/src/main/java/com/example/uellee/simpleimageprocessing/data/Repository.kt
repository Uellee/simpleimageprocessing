package com.example.uellee.simpleimageprocessing.data

import android.content.Context
import android.net.Uri
import android.util.SparseArray
import com.example.uellee.simpleimageprocessing.domain.IRepository
import com.example.uellee.simpleimageprocessing.domain.model.Transformation
import com.example.uellee.simpleimageprocessing.presentation.transform
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by DEA on 20.12.17.
 */
@Singleton
class Repository @Inject constructor(val context: Context,
                                     private val responseResolver: IResponseResolver,
                                     private val fileManager: IFileManager
) : IRepository {

    override var responseSource: IResponseSource? = null
        set(value) {
            responseResolver.responseSource = value
        }

    private val transformationsMap = SparseArray<Transformation>()

    override val cachedTransformations: List<Pair<Int, Transformation>>
        get() = buildArrayList(transformationsMap)
    private var id = 0
        get() = field++
        set(value) {
            if (field < value)
                field = value
        }

    private var currentImageUri = ""
        set(value) {
            if (value != "") imageUriSubject.onNext(value)
            field = value
        }
    private val imageUriSubject = BehaviorSubject.create<String>()
    override val imageStream: Observable<String>
        get() = imageUriSubject.hide()
    override val tempImageUri: String
        get() = fileManager.provideTempImageFileUri().toString()

    private val transformationSubject = PublishSubject.create<Pair<Int, Transformation>>()

    override val appliedTransformationsStream: Observable<Pair<Int, Transformation>>
        get() = transformationSubject.hide()

    init {
        responseResolver.contentUriStream
                .map { it.toString() }
                .subscribe {
                    currentImageUri = it
                    transformationsMap.clear()
                }
    }

    override fun applyTransformation(transformation: Transformation) {
        (id to transformation).let {
            transformationsMap.put(it)
            transformationSubject.onNext(it)
        }
    }

    override fun removeTransformation(id: Int) = transformationsMap.remove(id)

    override fun setTransformedImageAsSource(id: Int) {
        val bitmap = fileManager.openBitmap(Uri.parse(currentImageUri))
        val transform = bitmap.transform(transformationsMap[id])

        val newUri = fileManager.saveImage(transform)

        currentImageUri = newUri.toString()
    }

}
