package com.example.uellee.simpleimageprocessing.presentation

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.uellee.simpleimageprocessing.data.IResponseSource
import com.example.uellee.simpleimageprocessing.data.Response
import com.example.uellee.simpleimageprocessing.di.ComponentsOwner
import com.example.uellee.simpleimageprocessing.di.activity.ActivityModule
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

abstract class BaseActivity : AppCompatActivity(), IResponseSource {

    private val responseObject = PublishSubject.create<Response>()
    override val responseStream: Observable<Response>
        get() = responseObject.hide()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ComponentsOwner.activityComponent = ComponentsOwner.appComponent
                .activityComponentBuilder()
                .activityModule(ActivityModule(this))
                .build()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        responseObject.onNext(Response(requestCode, resultCode, data))
    }
}
