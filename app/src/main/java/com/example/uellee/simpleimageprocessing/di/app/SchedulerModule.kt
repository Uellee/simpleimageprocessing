package com.example.uellee.simpleimageprocessing.di.app

import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by DEA on 25.12.17.
 */
@Module
class SchedulerModule {
    @Provides
    @Singleton
    @Named("CachedThreadPoolExecutor")
    fun provideCachedThreadPoolExecutor(): Executor = Executors.newCachedThreadPool()

    @Provides
    @Singleton
    @Named("SingleThreadPoolExecutor")
    fun provideSingleThreadPoolExecutor(): Executor = Executors.newSingleThreadExecutor()

    @Provides
    @Singleton
    @Named("CachedThreadPoolScheduler")
    fun provideCachedThreadPoolScheduler(
            @Named("CachedThreadPoolExecutor") executor: Executor
    ): Scheduler = Schedulers.from(executor)

    @Provides
    @Singleton
    @Named("SingleThreadPoolScheduler")
    fun provideSingleThreadPoolScheduler(
            @Named("SingleThreadPoolExecutor") executor: Executor
    ): Scheduler = Schedulers.from(executor)
}