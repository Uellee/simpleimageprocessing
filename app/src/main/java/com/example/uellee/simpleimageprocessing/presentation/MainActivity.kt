package com.example.uellee.simpleimageprocessing.presentation

import android.os.Bundle
import com.example.uellee.simpleimageprocessing.R
import com.example.uellee.simpleimageprocessing.presentation.workspace.WorkspaceFragment

class MainActivity : BaseActivity() {

    private val WORKSPACE = "workspace fragment"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val fragment = supportFragmentManager.findFragmentByTag(WORKSPACE) ?: WorkspaceFragment()
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, fragment, WORKSPACE)
                .commit()

    }
}
