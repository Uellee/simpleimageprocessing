package com.example.uellee.simpleimageprocessing.di.activity

import javax.inject.Scope

/**
 * Created by DEA on 20.12.17.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope