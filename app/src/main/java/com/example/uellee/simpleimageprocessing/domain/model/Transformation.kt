package com.example.uellee.simpleimageprocessing.domain.model

/**
 * Created by DEA on 19.12.17.
 */
enum class Transformation {
    GRAY_SCALE, MIRROR, POSITIVE_ROTATION
}