package com.example.uellee.simpleimageprocessing.presentation.transformationslist

import android.graphics.Bitmap
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.example.uellee.simpleimageprocessing.R
import com.example.uellee.simpleimageprocessing.presentation.PlaceholderImageView
import com.example.uellee.simpleimageprocessing.presentation.layoutInflater
import io.reactivex.Single

/**
 * Created by DEA on 31.12.17.
 */
class TransformationsAdapter(val transformations: MutableList<Pair<Int, Single<Bitmap?>>>,
                             private val onItemClick: (Int) -> Unit = {},
                             private val onRemove: (Int) -> Unit = {})
    : RecyclerView.Adapter<TransformationsAdapter.TransformationVH>() {

    override fun onBindViewHolder(holder: TransformationVH, position: Int) {
        holder.image.reset()
        bindVH(holder, position)

        if (position % 2 == 1) holder.v.setBackgroundColor(Color.parseColor("#eeeeee"))
        else holder.v.setBackgroundColor(Color.parseColor("#ffffff"))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransformationVH {
        val view = parent.context.layoutInflater.inflate(
                R.layout.vh_transformations_list_item, parent, false)

        val vh = TransformationVH(view)

        vh.image.setOnClickListener {
            if (vh.lManagePanel.visibility == View.VISIBLE) {
                vh.lManagePanel.visibility = View.GONE
            } else {
                vh.lManagePanel.visibility = View.VISIBLE
            }
        }

        vh.bRemoveItem.setOnClickListener { onRemove(vh.v.tag as Int) }
        vh.bSelectItem.setOnClickListener { onItemClick(vh.v.tag as Int) }


        return vh
    }

    override fun getItemCount(): Int {
        return transformations.size
    }

    fun addItem(item: Pair<Int, Single<Bitmap?>>) {
        transformations.add(transformations.size, item)
        notifyItemInserted(transformations.size - 1)
    }

    fun removeItem(id: Int) {
        transformations.removeWhich { it.first == id }
        notifyDataSetChanged()
    }

    private fun bindVH(holder: TransformationVH, position: Int) {
        holder.lManagePanel.visibility = View.GONE
        holder.v.tag = transformations[position].first
        holder.image.setBitmapSource(transformations[position].second)
    }

    fun clear() {
        transformations.clear()
        notifyDataSetChanged()
    }

    class TransformationVH(val v: View) : RecyclerView.ViewHolder(v) {
        val image: PlaceholderImageView = v.findViewById(R.id.ph_transformed_image)
        val lManagePanel: View = v.findViewById(R.id.l_manage_panel)
        val bRemoveItem: View = v.findViewById(R.id.i_b_remove_item)
        val bSelectItem: View = v.findViewById(R.id.i_b_source_item)
    }
}

fun <T, L> MutableList<Pair<T, L>>.removeWhich(predicate: (Pair<T, L>) -> Boolean) {
    forEach {
        if (predicate(it)) {
            remove(it)
            return
        }
    }
}