package com.example.uellee.simpleimageprocessing.di.app

import com.example.uellee.simpleimageprocessing.di.activity.ActivityComponent
import dagger.Component
import javax.inject.Singleton

/**
 * Created by DEA on 18.12.17.
 */
@Singleton
@Component(modules = [AppModule::class,
    FileManagerModule::class,
    ResponseResolverModule::class,
    SchedulerModule::class])
interface AppComponent {
    fun activityComponentBuilder(): ActivityComponent.Builder
}