package com.example.uellee.simpleimageprocessing

import android.app.Application
import com.example.uellee.simpleimageprocessing.di.ComponentsOwner
import com.example.uellee.simpleimageprocessing.di.app.AppModule
import com.example.uellee.simpleimageprocessing.di.app.DaggerAppComponent

/**
 * Created by DEA on 19.12.17.
 */
class SIPApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        ComponentsOwner.appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(applicationContext)).build()

    }
}