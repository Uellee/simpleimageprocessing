package com.example.uellee.simpleimageprocessing.di.workspace

import javax.inject.Scope

/**
 * Created by DEA on 18.12.17.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class WorkspaceScope