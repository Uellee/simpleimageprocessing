package com.example.uellee.simpleimageprocessing.data

import android.graphics.Bitmap
import android.net.Uri

/**
 * Created by DEA on 24.12.17.
 */
interface IFileManager {
    val cacheUri: Uri?
    fun provideTempImageFileUri(): Uri
    fun saveImage(bitmap: Bitmap): Uri
    fun openBitmap(uri: Uri): Bitmap
}