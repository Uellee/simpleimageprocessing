package com.example.uellee.simpleimageprocessing.di.activity

import com.example.uellee.simpleimageprocessing.data.Repository
import com.example.uellee.simpleimageprocessing.domain.IRepository
import dagger.Binds
import dagger.Module

/**
 * Created by DEA on 20.12.17.
 */
@Module
interface RepositoryModule {
    @Binds
    @ActivityScope
    fun bindRepository(repository: Repository): IRepository
}