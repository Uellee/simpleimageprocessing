package com.example.uellee.simpleimageprocessing.data

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Rect
import android.net.Uri
import android.support.v4.content.FileProvider
import java.io.File
import java.io.FileOutputStream
import javax.inject.Inject

/**
 * Created by DEA on 24.12.17.
 */
class FileManager @Inject constructor(val context: Context) : IFileManager {
    private var lastProvidedUri: Uri? = null

    override val cacheUri: Uri?
        get() = lastProvidedUri

    override fun provideTempImageFileUri(): Uri {
        val uriForFile = FileProvider.getUriForFile(
                context,
                "com.example.fileprovider",
                createImageFile())
        lastProvidedUri = uriForFile
        return uriForFile
    }

    private fun createImageFile(suffix: String = ".jpg"): File {
        val dir = File(context.cacheDir, "pictures")
        dir.mkdir()
        return File.createTempFile("IMG_", suffix, dir)
    }

    override fun saveImage(bitmap: Bitmap): Uri {
        val imageFile = createImageFile(".png")
        val outputStream = FileOutputStream(imageFile)
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
        outputStream.close()

        return FileProvider.getUriForFile(context, "com.example.fileprovider", imageFile)
    }

    override fun openBitmap(uri: Uri): Bitmap {
        val options = BitmapFactory.Options()
        options.inSampleSize = 1

        val stream = context.contentResolver.openInputStream(uri)

        return BitmapFactory.decodeStream(
                stream, Rect(-1, -1, -1, -1), options)
    }
}
