package com.example.uellee.simpleimageprocessing.domain

/**
 * Created by DEA on 20.12.17.
 */
interface IImplicitRouter {
    fun dispatchImageIntent()
    fun dispatchCameraIntent(tempImageFileUri: String)
}